import java.io.File;
import java.io.IOException;

public class CreateFile {
    public static void main(String[] args)
    {
        try {

            File newfile = new File("D:\\newfile.txt");
            if (newfile.createNewFile()) {
                System.out.println("New file has been created" + newfile.getName());
            } else {
                System.out.println("file already existed");
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
